import pandas as pd
import numpy as np
from austinPython.date_conversion import date_to_unix
from datetime import datetime

from austinPython.save_excel import save_excel




# 1. Prepare Data
# 1.1. Load datasets
austin_reservoirs_meta = pd.read_excel('bucketsIndex/Austin_4reservoirs.xlsx')
austin_budget = pd.read_excel('bucketsIndex/Austin_budget.xlsx')
austin_evap = pd.read_excel('bucketsIndex/Austin_Evaporation.xlsx')
austin_demand = pd.read_csv('bucketsIndex/Austin_pumpage.csv')
austin_storage = pd.read_excel('bucketsIndex/Austin_reservoir_storage.xlsx')
austin_rainfall = pd.read_excel('bucketsIndex/Austin_precipitation.xlsx')
austin_consumption = pd.read_excel('bucketsIndex/Austin_ConsumptionTotal.xlsx')

# 1.2. Match Timestamp Frequency

### Rainfall & Evap

# Merge year and month columns, conver into datestamp
def convert_date(dataset):

    if 'date' not in dataset.columns:
        # If month's organized in columns, melt into rows
        if 'ANNUAL' in dataset.columns:
            print('here!')
            dataset = dataset.drop(columns=['ANNUAL'])
            dataset = pd.melt(dataset, id_vars=[ "YEAR"],
                          var_name="month", value_name="Value")
            dataset = dataset.sort_values(by=['YEAR'])
            dataset['year'] = dataset['YEAR']
            dataset = dataset.drop(columns=['YEAR'])

        def set_date(row):
            date = str(row['year']) + ' ' + str(row['month'])
            return pd.to_datetime(date)

        dataset['date'] = dataset.apply(set_date, axis=1)
        dataset = dataset.drop(columns=['month', 'year'])
        dataset = dataset.reset_index()

        return dataset





## Rainfall & evap
# Inflow through rain, Outflow through evaporation
# One inch of rain falling on 1 acre of ground is equal to about 27,154 gallons


RC = np.sum(austin_reservoirs['ConservationCapacity(acre-ft)'])
reservoir_surface_area = np.sum(austin_reservoirs['SurfaceAreaacres'])


# Melt month columns to rows
austin_evap = convert_date(austin_evap)
austin_rainfall = convert_date(austin_rainfall)
# Rename columns
austin_evap.columns = ['evaporation']
austin_rainfall.columns = ['rainfall']

# Trim
austin_evap = austin_evap.drop(columns=['index']).set_index('date').sort_index()


austin_rainfall['gallons_rain'] = np.multiply(reservoir_surface_area * 27,154 ) * austin_rainfall['rainfall']
austin_evap['gallons_evap'] = np.multiply(reservoir_surface_area * 27,154 ) * austin_evap['evaporation']


### Consumption
austin_consumption = convert_date(austin_consumption)

### Budget & Pumpage/demand
austin_budget['date'] = pd.to_datetime(austin_budget['year'], format='%Y')
austin_demand['date'] = pd.to_datetime(austin_demand['year'], format='%Y')


# Grab relevant columns and make master dataframe

# trimmings
austin_rainfall = austin_rainfall.set_index('date').drop(columns=['index'])
austin_consumption = austin_consumption.drop(columns=['index']).set_index('date')
austin_budget = austin_budget.set_index('date')
austin_budget = austin_budget.drop(columns=['Source(numbers in millions)'])
austin_demand = austin_demand.drop(columns=['year']).set_index('date')
austin_storage = austin_storage.set_index('date')
austin_consumption = austin_consumption.sort_index()






# Total total consumption
austin_consumption.head()

austin_consumption_dates = set(austin_consumption.index)
np.sort(austin_consumption_dates, 'index')

austin_consumption_total = pd.DataFrame(index=austin_consumption_dates).sort_index()
for row_index in austin_consumption_dates:
    austin_consumption_total.loc[row_index, 'aggregated_revenue'] = np.sum(austin_consumption.loc[row_index]['revenue'])
    austin_consumption_total.loc[row_index, 'aggregated_gallons'] = np.sum(austin_consumption.loc[row_index]['total_gallons'])




# Join datasets
df = pd.DataFrame()

datasets = [austin_demand, austin_budget, austin_consumption_total, austin_rainfall, austin_evap]


df = pd.concat(datasets, axis=1, join='outer')
df = df.join(austin_storage) # Storage has daily data, join as such to grab only monthly

# From year 2000 onward
df = df[df.index >= '2000-01-01']


save_excel(df, 'Austin_MASTER2')



# austin_evap = austin_evap.sort_index()
# austin_rainfall = austin_rainfall.sort_index()


#################

import pandas as pd
import numpy as np
from austinPython.date_conversion import date_to_unix
from datetime import datetime

from austinPython.save_excel import save_excel




# 1. Prepare Data
# 1.1. Load MASTER Dataset
df = pd.read_excel('bucketsIndex/Austin_MASTER2.xlsx')




# Aggregate monthly revenue and water demand by year
import datetime as dt

yrev = []
ygal = []
yearly_index = []
for year in range(2012, 2017):

    start = df.index.searchsorted(dt.datetime(year, 1, 1))
    end = df.index.searchsorted(dt.datetime(year, 12, 31))

    df.loc[df.index[start], 'yearly_revenue'] = int(np.sum(df.aggregated_revenue.iloc[start:end]))
    df.loc[df.index[start], 'yearly_gallons'] = int(np.sum(df.aggregated_gallons.iloc[start:end]))


    yearly_index.append(df.index[start])
    yrev.append(int(np.sum(df.aggregated_revenue.iloc[start:end])))
    ygal.append(int(np.sum(df.aggregated_gallons.iloc[start:end])))




df_monthly = df.loc[yearly_index]
save_excel(df, 'Austin_MASTER3')

df_monthly['diff_revenue_%'] = 1 - (df_monthly['BudgetedRevenue'] * 1000000 / df_monthly['yearly_revenue'])
df_monthly['diff_gallons_%'] = 1 - (df_monthly['Pumpage']  / df_monthly['yearly_gallons'])



# 2 Drawdown Flow index
# Not all inflow is accounted for, deficit therefore seems to increase with time

df = pd.read_excel('bucketsIndex/Austin_MASTER3.xlsx')





df['withdrawal_balance'] = df.aggregated_gallons + df.gallons_evap

np.maximum(df['withdrawal_balance'] - df['gallons_rain'],0)
df['Accumulated_deficit'] = np.maximum(df['withdrawal_balance'] - df['inflow'] - df['gallons_rain'],0)

pd.DataFrame([np.cumsum(df.withdrawal_balance), np.cumsum(df.gallons_rain), df['Accumulated_deficit']])



## 3 Filling Flow index

# Conversion
def to_gallon(n):
    return n * 325851
def to_acrefeet(n):
    return n / 325851

# Conversion
def ft3s_to_GalMo(n):
    return n * 19389506

## Inflow
austin_discharge = pd.read_excel('bucketsIndex/Austin_RiverDischarge_ft3s.xlsx')
austin_discharge = convert_date(austin_discharge)
austin_discharge = austin_discharge.drop(columns=['index']).set_index('date').sort_index()
austin_discharge['GalMo'] = ft3s_to_GalMo(austin_discharge.Value)
INFLOW = austin_discharge['GalMo'][(austin_discharge.index >= '2012-01-01') & (austin_discharge.index <= '2016-12-01')]
# df['inflow'] = INFLOW





df['Filling_flow'] = np.maximum(df['gallons_rain'] + df['inflow'] - df['withdrawal_balance'], 0)



# Conservation capacity seems to alternate by months at a time, suggesting that it encompasses different reservoirs are considered at different time periods
df['spillage'] = np.maximum(df.conservation_capacity[-1] - df.conservation_storage[0] + np.cumsum(df['Filling_flow'] - np.cumsum(df['Accumulated_deficit'])), 0)

df['Filling_flow_correctedforspillage'] = np.maximum(df['gallons_rain'] - df['withdrawal_balance'] - df['spillage'], 0)


df['current_storage'] = df.conservation_storage[0] + np.cumsum(df['Filling_flow']) - np.cumsum(df['Accumulated_deficit'])


df.conservation_capacity
df.percent_full
df.conservation_storage
df['reservoir_storage(af)']


to_acrefeet(df['current_storage']) / df['reservoir_storage(af)']
