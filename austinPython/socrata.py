#!/usr/bin/env python

# make sure to install these packages before running:
# pip install pandas
# pip install sodapy

# https://data.austintexas.gov/profile/pguzman/hkmi-ghxc/app_tokens

import pandas as pd


consumption_residential = pd.read_excel('bucketsIndex/Austin_ConsumptionResidential.xlsx')
consumption_commercial = pd.read_excel('bucketsIndex/Austin_ConsumptionCommercial.xlsx')


def organize_consumption_data(results_df):
    years = []
    months = []
    for i,each in enumerate(results_df['year_month'].astype(str)):
        print(i, each)
        years.append(each[0:4])
        months.append(each[-2:])

    results_df['year'] = years
    results_df['month'] = months

    return results_df

consumption_commercial = organize_consumption_data(consumption_commercial)
consumption_residential = organize_consumption_data(consumption_residential)

consumption_total = pd.concat([consumption_residential, consumption_commercial])
consumption_total = consumption_total.drop('year_month', axis=1).reset_index()
consumption_total = consumption_total.reset_index()
consumption_total = consumption_total.sort_values(['year', 'month', 'postal_code', 'customer_class'], ascending=[True, True, True, True])

rates = consumption_commercial = pd.read_excel('bucketsIndex/rates.xlsx')

total = []
for row in consumption_total:
    print(row)
    customer = row['customer_class']
    rate = float(rates['rate_per_1000Gal'].loc[rates['customer_type'] == customer].values[0])
    total = consumption_total.loc[consumption_total['customer_class'] == customer]['total_gallons'] * rate
    print(total)


def save_excel(df, filename):
    from pandas import ExcelWriter
    writer = ExcelWriter('bucketsIndex/' + filename + '.xlsx')
    df.to_excel(writer,'Sheet1')
    writer.save()





def rate_set(row):
    customer = row['customer_class']
    rate = float(rates['rate_per_1000Gal'].loc[rates['customer_type'] == customer].values[0])
    return rate

consumption_total['rate'] = consumption_total.apply(rate_set, axis=1)

consumption_total['revenue'] = np.multiply(consumption_total.rate, consumption_total.total_gallons)



