from bokeh.io import output_file, show
from bokeh.layouts import widgetbox
from bokeh.models.widgets import Button, RadioButtonGroup, Select, Slider, DateRangeSlider
import pandas as pd
from datetime import datetime

output_file("layout_widgets.html")

# create some widgets


date_start = pd.to_datetime('2012-01-01')
date_end = pd.to_datetime('2017-01-01')

def date_range_update(attrname, old, new):
    print('-- range values:', date_slider.value)
    # Works
    d1 = datetime.fromtimestamp(date_slider.value[0] / 1000)
    # Does not Work, gives error
    d2 = datetime.fromtimestamp(date_slider.value[0])

date_slider = DateRangeSlider(start=date_start, end=date_start, value=(date_start,date_end), step=1, title="Contract Period")
date_slider.on_change('value', date_range_update)

slider = Slider(start=0, end=100, value=1, step=1, title="Number of contracts")
button_group = RadioButtonGroup(labels=["a) Surcharges", "b) DUBSO policy", "c) Contingency Funds"], active=0)
#button_group.on_change('value', date_range_update)

select = Select(title="Reservoir Level:", value="D1 (80%)", options=["D1 (80%)", "D2 (60%)", "D3 (40%)", "D4 (>30%, emergency)"])
button_1 = Button(label="Reset")
select2 = Select(title="Phase adjustment:", value="foo", options=["Dynamic time warping", "Convolution", "Correlation", "Autocorrelation"])

button_2 = Button(label="Optimize!")

# put the results in a row
show(widgetbox( button_1, slider, button_group, select, select2, button_2, width=300))
show(widgetbox( button_1, date_slider, width=300))


Dynamic time warping
Convolution
Correlation
Autocorrelation




##############

from bokeh.io import output_file, show
from bokeh.layouts import column
from bokeh.plotting import figure

output_file("layout.html")

x = list(range(11))
y0 = x
y1 = [10 - i for i in x]
y2 = [abs(i - 5) for i in x]

# create a new plot
s1 = figure(plot_width=250, plot_height=250, title=None)
s1.circle(x, y0, size=10, color="navy", alpha=0.5)

# create another one
s2 = figure(plot_width=250, plot_height=250, title=None)
s2.triangle(x, y1, size=10, color="firebrick", alpha=0.5)

# create and another
s3 = figure(plot_width=250, plot_height=250, title=None)
s3.square(x, y2, size=10, color="olive", alpha=0.5)

# put the results in a column and show
show(column(s1, s2, s3))