
import pandas as pd

from bokeh.io import output_notebook, show
from bokeh.plotting import figure
output_notebook()


df = pd.read_excel('bucketsIndex/Austin_MASTER3.xlsx')

import numpy as np
import pandas as pd

from bokeh.plotting import figure, show, output_file
from bokeh.palettes import brewer


df = pd.read_excel('bucketsIndex/Austin_MASTER3.xlsx')[['conservation_storage', 'Filling_flow', 'Accumulated_deficit', 'current_storage']]


from bokeh.layouts import gridplot

x = df.index
y0 = df.Filling_flow
y1 = df.Accumulated_deficit
y2 = df.current_storage

TOOLS = "pan,wheel_zoom,box_zoom,reset,save"

plot_options = dict(width=1000, plot_height=500, tools=TOOLS, x_axis_type='datetime')
# create a new plot
s1 = figure(**plot_options, title='Filling_flow' )
s1.line(x, y0, line_width=2, color="navy")
# create a new plot and share both ranges
#s2 = figure(x_range=s1.x_range, **plot_options, title = 'Accumulated_deficit')
s1.line(x, y1, line_width=2, color="firebrick")
# create a new plot and share only one range
#s3 = figure(x_range=s1.x_range, **plot_options, title = 'current_storage')
s1.line(x, y2, line_width=2, color="olive")
p = gridplot([[s1]]) #, [s2], [s3]])
# show the results
show(p)





##########
def  stacked(df):
    df_top = np.cumsum(df.Filling_flow)
    df_bottom = np.cumsum(df.Accumulated_deficit)
    df_stack = pd.concat([df_bottom, df_top], ignore_index=True)
    return df_stack

areas = stacked(df)
colors = brewer['Spectral'][4]
x2 = np.hstack((df.index[::-1], df.index))

p = figure(**plot_options)
p.grid.minor_grid_line_color = '#eeeeee'

p.patches([x2] * areas.shape[1], [areas[c].values for c in areas],
          color=colors, alpha=0.8, line_color=None)

output_file('brewer.html', title='brewer.py example')

show(p)

############