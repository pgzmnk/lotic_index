{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimizing drought hedge index contract for a reservoir\n",
    "\n",
    "Water utilities often operate under a cost-recovery budgetary model, with a significant portion of revenue coming from the volumetric sale of water. In times of drought, a utility might be hesitant to implement water-use restrictions, as reductions in water sales can leave a water utility financially vulnerable because revenue losses might be difficult to plan for and absorb.\n",
    "\n",
    "Among water resource planning strategies, financial instruments such as drought-related compensatory payments propose to mitigate the effects of large intermittent revenue losses. \n",
    "\n",
    "By creating indexes that closely follow financial performance under unknown future weather conditions one can create financial insurance contracts. Such potential contracts contract are evaluated based on their ability to minimize basis risk by returning payouts of the appropriate magnitude during the years in which revenue losses are experienced.\n",
    "\n",
    "\n",
    "<font color=blue>This IPython Notebook provides an interactive way to follow the outlined index and policy methodology. The intent behind this notebook is to present the concepts in a distilled, convenient, and effective way. Further readings and references appear in the last section of the document.</font>\n",
    "<br />\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Risk Transfer Analysis Walkthrough\n",
    "\n",
    "    Introduction\n",
    "    Case Study: Austin Water\n",
    "    Walkthrough:\n",
    "        I) Acquiring financial loss data\n",
    "            a) Retrieve datasets and models\n",
    "            b) Preprocess data\n",
    "            c) Homogenize timestamps\n",
    "        II) Index Generation\n",
    "            a) Drawdown Flow Index\n",
    "            b) Filling Flow Index\n",
    "            c) Current Storage index\n",
    "            d) Determine Current Water Storage Level (Si)\n",
    "        III) Policy generation\n",
    "            a) Explore Policy types\n",
    "            b) Adjust Thresholds (Zm, Z*m)\n",
    "        IV) Evaluate & Optimize Policy Performance\n",
    "            a) Calculate Total Payouts\n",
    "            b) Calculate Total Revenue Losses\n",
    "            c) Calculate Contract Price\n",
    "            d) Optimize for number of contracts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Third-party index insurance contracts, either independently or in combination with more traditional hedging tools, can provide an effective means of reducing a utility’s financial vulnerability to drought. By fundamenting policies on predetermined high-correlation indexes, a utility can manage revenue risk through smart contract policies and, in turn, participate in a prediction market to transact water supply risk.\n",
    "\n",
    "The workflow to conduct a hydrologic risk transfer analysis can be outlined in 4 categories:\n",
    "\n",
    "**I) Acquiring financial loss data**\n",
    "\n",
    "For the asset in question we first determine the risk metric that describes financial losses. A straightforward risk metric can be the revenue of the asset within a given time interval (e.g. monthly or yearly), or the percentage difference between forecasted and actual revenues.\n",
    "\n",
    "Once the risk metric is selected, simulations of financial performance are conducted using historical and synthetic time-series data representative of supply and demand drivers and cost and price inputs.\n",
    "\n",
    "**II) Index Generation**\n",
    "\n",
    "When generating an index, the goal is to construct and weight a set of publicly available and verifiable time-series variables that closely correlate to financial performance. This ad hoc approach relies on a heuristic to limit the search space of potential variables.\n",
    "\n",
    "The ultimate selection of index variables is driven by variable selection approaches such as PCA, LASSO, etc. The indexes emerging with tightest correlations from training and testing runs then continue on to inform the policy generation. The high-correlation of the index to financial losses is the first component to ensure a low basis risk for the final policy.\n",
    "\n",
    "**III) Policy generation**\n",
    "\n",
    "Policies are generated combining candidate indices with contract structures such as insurance (flat premiums, payouts varying with index), binary (single payout at indextreshold), and collar (premiums and payouts vary with index). The index-contract combinations are the calibrated iteratively for various parameters such as strike level, min and max payout, premier cost, and deductible amounts. \n",
    "\n",
    "**IV) Policy performance**\n",
    "\n",
    "Calibrated policies are evaluated for performance based on their delivery of mutual benefit to the hypothetical insurer and insured. Policy performance is simulated using the testing dataset and measured by a range of important criteria, such as basis risk; premia as % of revenues; loading, the amount by which premium exceeds expected value of payouts; coverage level, the ratio of minimum losses between insured and uninsured scenarios; and cost, the reduction of average revenues/profit when contract is in place. The ultimate goal is to generate a contract that reduces the level of basis risk (lack of correlation between payouts and losses) while still providing reasonable cost to the insured and capital returns to the insurer. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Data** \n",
    "\n",
    "The data required for risk transfer analysis spans environmental systems, physical infrastructure, and markets. LoticLabs assembles and manages the data in an internal integration and warehousing system called DataDocs. Data is structured as a semantic workflow modeling environment that, by integrating and harmonizing data, facilitates quick analysis, automatization, and natural language queries.\n",
    "\n",
    "For the particular analysis using a reservoir level index the following datasets are needed. Their source and significance are explained below. The table indicates which variables can be adjusted in order to optimize revenue through policy. \n",
    "\n",
    "| Dataset | Variable Name |Adjustable|\n",
    "|------|------|------|\n",
    "|Reservoir Evaporation|Ev||\n",
    "|Reservoir Rainfall|Rf||\n",
    "|Reservoir Release|R||\n",
    "|Demand|D||\n",
    "|Demand Tiers|DT|X|\n",
    "|Inflow|I||\n",
    "|Spillage|W||\n",
    "|Reservoir Capacity|RC||\n",
    "|Initial Reservoir Storage|So||\n",
    "|Reservoir Storage at the start of the contract|S||\n",
    "|‘Down-and-in’ Storage Threshold used to apply water use restrictions (MG)|Zm|X|\n",
    "|‘up-and-out’’ Storage Threshold used to lift water use restrictions (MG)|Z*m|X|\n",
    "|Number of contracts|n|X|\n",
    "|Revenue|Revenue|_|\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Case Study: _Austin Water_\n",
    "\n",
    "Water in Austin is currently stored in four reservoirs (Buchanan, Georgetown, Stillhouse Hollow and Travis) which are fed mainly by the inflow of the Colorado River.\n",
    "\n",
    "A multireservoir model provides information on historical water levels. The scale and timing of droughts and their respective financial effects are simulated using detailed data derived from utility billing records.\n",
    "\n",
    "|Reservoir|\tConservation Capacity(acre-ft)|\tSurface Area (acres)|\n",
    "|------|------|------|\n",
    "|Buchanan|\t860607|\t21185|\n",
    "|Georgetown|\t36823|\t1020|\n",
    "|StillhouseHollow|\t227771|\t5974|\n",
    "|Travis|\t1113348|\t16376|\n",
    "|**Total:**|\t**2238549**|\t**44555**|\n",
    "\n",
    "\n",
    "<br/>\n",
    "<font color=blue>\n",
    "Data hints at the fact that historically the reservoirs may have not all been used at once, with different Total Conservation Capacity attributed to different years. </font>\n",
    "\n",
    "Customers of the City of Austin Water Utility receive their drinking water from two water treatment plants that pump surface water from the Colorado River as it flows into Lake Austin.\n",
    "\n",
    "![texas](https://waterdatafortexas.org/reservoirs/individual/austin/location.png)\n",
    "\n",
    "## Austin Datasets\n",
    "\n",
    "_[CentralTexasHub](http://www.centraltexashub.org/)_ > __[LCRA](https://www.lcra.org/water/water-supply/water-supply-contracts/)_ > _[AustinWater](http://www.austintexas.gov/department/water)\n",
    "\n",
    "The Central Texas Hub continuously ingests water observations data from the USGS, LCRA and the City of Austin into a central database maintained by _[CRWR](http://efis.crwr.utexas.edu/hydrology.php)_. The KISTERS WISKI database is used for this purpose and WISKI is also used by the City of Austin and by LCRA to store their observations data internally within their own organizations. Observations data from the USGS is ingested using water web services in WaterML, a special language designed to communicate water data through the internet. The data are mapped thematically as streamflow, precipitation and water level information and charts are provided for viewing the data over a week, a month or a year. Comparison with historical statistics for those locations is provided.\n",
    "\n",
    "\n",
    "\n",
    "<font color=red>[For the purpose of this case study, data was downloaded as spreadsheets from CentralTexasHub databases. It was preprocessed into a master table indexed by monthly intervals.]</font>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Walkthrough:\n",
    "\n",
    "# I) Acquiring financial loss data\n",
    "    a) Retrieve datasets and models\n",
    "    b) Preprocess data\n",
    "    c) Homogenize timestamps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## a) Retrieve datasets and models\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## _Data CheckList_\n",
    "\n",
    "| Dataset | Variable Name |File Name| Source|\n",
    "|------|------|------|------|\n",
    "|Reservoir Evaporation|Ev|Austin_Evaporation.xlsx| http://www.twdb.texas.gov/surfacewater/conditions/evaporation/|\n",
    "|Reservoir Rainfall|P|Austin_precipitation|http://www.twdb.texas.gov/surfacewater/conditions/evaporation/|\n",
    "|Reservoir Release|R|_<font color=red>unk</font>_||\n",
    "|Demand|D|Austin_ConsumptionTotal.xlsx|\n",
    "|Demand Tiers|DT|X|\n",
    "|Inflow|I|Austin_RiverDischarge_ft3s.xlsx|_[USGS](https://waterdata.usgs.gov/nwis/monthly?referred_module=sw&amp;site_no=08158000&amp;por_08158000_136117=1389,00060,136117,1898-03,2017-10&amp;start_dt=2010-01&amp;end_dt=2017-10&amp;format=html_table&amp;date_format=YYYY-MM-DD&amp;rdb_compression=file&amp;submitted_form=parameter_selection_list)_|\n",
    "|Spillage|W|_<font color=red>unk</font>_||\n",
    "|Reservoir Capacity|RC|Austin_reservoir_storage.xlsx|https://waterdatafortexas.org/reservoirs/municipal/austin\n",
    "|Initial Reservoir Storage|So|Austin_reservoir_storage.xlsx|\n",
    "|Reservoir Storage at the start of the contract|S|Austin_reservoir_storage.xlsx|\n",
    "|Rates|Rates|Austin_rates.xlsx|http://www.austintexas.gov/department/austin-water-utility-service-rates|\n",
    "|Revenue|Revenue|_|\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "from beakerx import *\n",
    "# beakerx.pandas_display_default()\n",
    "\n",
    "\n",
    "##%matplotlib inline\n",
    "# Javascript to Show/Hide\n",
    "from IPython.display import HTML\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load Datasets\n",
    "austin_reservoirs = pd.read_excel('../bucketsIndex/Austin_4reservoirs.xlsx')\n",
    "austin_budget = pd.read_excel('../bucketsIndex/Austin_budget.xlsx')\n",
    "austin_demand = pd.read_csv('../bucketsIndex/Austin_pumpage.csv')\n",
    "austin_evap = pd.read_excel('../bucketsIndex/Austin_Evaporation.xlsx')\n",
    "austin_storage = pd.read_excel('../bucketsIndex/Austin_reservoir_storage.xlsx')\n",
    "austin_rainfall = pd.read_excel('../bucketsIndex/Austin_precipitation.xlsx')\n",
    "austin_consumption = pd.read_excel('../bucketsIndex/Austin_ConsumptionTotal.xlsx')\n",
    "austin_discharge = pd.read_excel('../bucketsIndex/Austin_RiverDischarge_ft3s.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. Austin Reservoirs\n",
    "Austin's water is supplied by 4 nearby reservoirs.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_4reservoirs.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. Austin Storage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_reservoir_storage.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Austin Total Consumption"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_ConsumptionTotal.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. Austin Budget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_budget.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5. Austin Evaporation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_Evaporation.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 6. Austin Storage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_reservoir_storage.xlsx')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 7. Austin Precipitation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pd.read_excel('../bucketsIndex/Austin_precipitation.xlsx')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 8. Austin Inflow (Colorado River)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Conversion\n",
    "def ft3s_to_GalMo(n):\n",
    "    return n * 19389506\n",
    "\n",
    "## Inflow\n",
    "pd.read_excel('../bucketsIndex/Austin_RiverDischarge_ft3s.xlsx')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## b) Preprocess data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Take relevant columns and make master dataframe\n",
    "\n",
    "# trimmings\n",
    "### austin_rainfall = austin_rainfall.drop(columns=['month', 'year']).set_index('date')\n",
    "# austin_evap = austin_evap.drop(columns=['month', 'year']).set_index('date')\n",
    "\n",
    "# austin_consumption = austin_consumption.drop(columns=['month', 'year']).set_index('date') # .drop(columns=['index'])\n",
    "# austin_storage = austin_storage.set_index('date')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=red>[For the purpose of the case study, preprocessing is not shown. Instead, a master dataset is presented.]</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "AUSTIN_MASTER = pd.read_excel('../bucketsIndex/Austin_MASTER3.xlsx')\n",
    "AUSTIN_MASTER.head()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## c) Homogenize Timestamps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_excel('../bucketsIndex/Austin_MASTER2.xlsx')\n",
    "\n",
    "\n",
    "# Aggregate monthly revenue and water demand by year\n",
    "import datetime as dt\n",
    "\n",
    "yrev = []\n",
    "ygal = []\n",
    "yearly_index = []\n",
    "for year in range(2012, 2017):\n",
    "\n",
    "    start = df.index.searchsorted(dt.datetime(year, 1, 1))\n",
    "    end = df.index.searchsorted(dt.datetime(year, 12, 31))\n",
    "\n",
    "    df.loc[df.index[start]]['yearly_revenue'] = int(np.sum(df.aggregated_revenue.iloc[start:end]))\n",
    "    df.loc[df.index[start]]['yearly_gallons'] = int(np.sum(df.aggregated_gallons.iloc[start:end]))\n",
    "\n",
    "\n",
    "    yearly_index.append(df.index[start])\n",
    "    yrev.append(int(np.sum(df.aggregated_revenue.iloc[start:end])))\n",
    "    ygal.append(int(np.sum(df.aggregated_gallons.iloc[start:end])))\n",
    "\n",
    "\n",
    "for year in range(2012, 2017):\n",
    "\n",
    "    start = df.index.searchsorted(dt.datetime(year, 1, 1))\n",
    "    end = df.index.searchsorted(dt.datetime(year, 12, 31))\n",
    "\n",
    "\n",
    "    df.loc[yearly_index]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=red>[For the purpose of the case study, Homogenizing  Timestamps is not shown. Instead, a subsequent master dataset is presented.]</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# II) Index Generation\n",
    "\n",
    "    a) Drawdown Flow Index\n",
    "    b) Filling Flow Index\n",
    "    c) Current Storage index\n",
    "    d) Determine Current Water Storage Level (Si)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## a) Drawdown Flow index "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# 2.1 Drawdown Flow index\n",
    "# Not all inflow is accounted for, deficit therefore seems to increase with time\n",
    "\n",
    "df = pd.read_excel('../bucketsIndex/Austin_MASTER3.xlsx')\n",
    "df['withdrawal_balance'] = df.aggregated_gallons + df.gallons_evap\n",
    "\n",
    "np.maximum(df['withdrawal_balance'] - df['gallons_rain'],0)\n",
    "df['Accumulated_deficit'] = np.maximum(df['withdrawal_balance'] - df['inflow'] - df['gallons_rain'],0)\n",
    "\n",
    "pd.DataFrame([np.cumsum(df.withdrawal_balance), np.cumsum(df.gallons_rain), df['Accumulated_deficit']])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## b) Filling Flow index "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [],
   "source": [
    "## 2.2 Filling Flow index\n",
    "\n",
    "# Conversion\n",
    "def to_gallon(n):\n",
    "    return n * 325851\n",
    "def to_acrefeet(n):\n",
    "    return n / 325851\n",
    "def ft3s_to_GalMo(n):\n",
    "    return n * 19389506\n",
    "\n",
    "df['Filling_flow'] = np.maximum(df['gallons_rain'] + df['inflow'] - df['withdrawal_balance'], 0)\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## c) Current Storage index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [],
   "source": [
    "## 2. Current Storage index\n",
    "\n",
    "# Conservation capacity seems to alternate by months at a time, suggesting that it encompasses different reservoirs are considered at different time periods\n",
    "df['spillage'] = np.maximum(df.conservation_capacity[-1] - df.conservation_storage[0] + np.cumsum(df['Filling_flow'] - np.cumsum(df['Accumulated_deficit'])), 0)\n",
    "\n",
    "df['Filling_flow_correctedforspillage'] = np.maximum(df['gallons_rain'] - df['withdrawal_balance'] - df['spillage'], 0)\n",
    "\n",
    "\n",
    "df['current_storage'] = df.conservation_storage[0] + np.cumsum(df['Filling_flow']) - np.cumsum(df['Accumulated_deficit'])\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{2194846, 2238549}"
      ]
     },
     "execution_count": 66,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "set(df.conservation_capacity)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## d) Determine Current Water Storage Level (Si)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# III) Policy generation\n",
    "    a) Explore Policy types\n",
    "    b) Adjust Thresholds (Zm, Z*m)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# IV) Evaluate & Optimize Policy Performance\n",
    "    a) Calculate Total Payouts\n",
    "    b) Calculate Total Revenue Losses\n",
    "    c) Calculate Contract Price\n",
    "    d) Optimize for number of contracts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Resources\n",
    "\n",
    "Plotting\n",
    "http://nbviewer.jupyter.org/github/bokeh/bokeh-notebooks/blob/master/index.ipynb\n",
    "\n",
    "iPython Resources http://nbviewer.jupyter.org/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2012-01-01 00:00:00 2016-12-01 00:00:00 \n",
      "\n",
      "Pumpage\n",
      "WaterServicePopulation\n",
      "PPCpd\n",
      "BudgetedRevenue\n",
      "BudgetedExpense\n",
      "ActualRevenue\n",
      "ActualExpense\n",
      "aggregated_revenue\n",
      "aggregated_gallons\n",
      "rainfall\n",
      "gallons_rain\n",
      "evaporation\n",
      "gallons_evap\n",
      "reservoir_storage(af)\n",
      "conservation_storage\n",
      "percent_full\n",
      "conservation_capacity\n",
      "withdrawal_balance\n",
      "Accumulated_deficit\n",
      "Filling_flow\n",
      "spillage\n",
      "Filling_flow_correctedforspillage\n",
      "current_storage\n",
      "inflow\n"
     ]
    }
   ],
   "source": [
    "import pandas as pd\n",
    "df2 = pd.read_excel('../bucketsIndex/Austin_MASTER3.xlsx')\n",
    "\n",
    "\n",
    "print(df2.index[0],df2.index[-1],'\\n')\n",
    "\n",
    "for column in df2.columns:\n",
    "    print(column)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
