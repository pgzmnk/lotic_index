# Datetime conversion
from datetime import datetime
import numpy as np
import pandas as pd
def date_to_unix(date_human): #  date_human = 'Jun 1 2005  1:33PM'
    if date_human == 'now':
        return datetime.now().replace(microsecond=0).timestamp()
    else:
        return datetime.strptime(date_human, '%b %d %Y %I:%M%p').replace(microsecond=0).timestamp()
def date_human_readable(date):

    if type(date) == list or type(date) == pd.core.series.Series:
        datelist = []
        for each in date:
            datelist.append(datetime.fromtimestamp(int(each)).strftime('%Y-%m-%d %H:%M:%S'))
        return datelist
    else:
        return datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d %H:%M:%S')
def date_range(date_start, date_end, period):
    return list(np.arange(date_start, date_end, period))