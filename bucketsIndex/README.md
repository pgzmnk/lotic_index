https://waterdatafortexas.org/drought

Glossary of terms
https://waterdatafortexas.org/reservoirs/glossary


Rates
http://www.austintexas.gov/department/austin-water-utility-service-rates


Google Maps
https://www.google.com.mx/maps/place/Buchanan+Lake/@30.4593684,-98.8068525,9z/data=!4m5!3m4!1s0x865af9e0edbb56fb:0x7f0e121dabc72896!8m2!3d30.7935256!4d-98.4293891

Evaporation and precipitation
http://www.twdb.texas.gov/surfacewater/conditions/evaporation/
https://waterdatafortexas.org/drought/evaporation-forecast

Reservoirs
https://waterdatafortexas.org/reservoirs/municipal/austin

Reservoir Storage projections
https://www.lcra.org/water/water-supply/highland-lakes-overview/Documents/DroughtUpdate_StorageProjection.pdf

Triggers
http://www.austintexas.gov/sites/default/files/files/Water/Conservation/Demand-Supply-EmergencyTriggers.pdf



LCRA
LCRA GIS of streamflow, lake level height, rainfall, etc. It seems to only show data from upto 14 days back.
https://hydromet.lcra.org/


Colorado River inflow
https://waterdata.usgs.gov/nwis/monthly?referred_module=sw&amp;site_no=08158000&amp;por_08158000_136117=1389,00060,136117,1898-03,2017-10&amp;start_dt=2010-01&amp;end_dt=2017-10&amp;format=html_table&amp;date_format=YYYY-MM-DD&amp;rdb_compression=file&amp;submitted_form=parameter_selection_list