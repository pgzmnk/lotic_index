# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This IPython Notebook provides an interactive way to follow the outlined index and policy methodology. The intent behind this notebook is to present the concepts in a distilled, convenient, and effective way. Further readings and references appear in the last section of the document. 

Version 1.1

### How do I get set up? ###

Analysis in
index_storage_level.ipynb

Folder 'bucketIndex' contains referenced datasets.


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Plinio Guzman
pgzmnk@gmail.com